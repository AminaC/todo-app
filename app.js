// select elements
const clear = document.querySelector(".clear");
const date = document.getElementById("date");
const list = document.getElementById("list");
const input = document.getElementById("input");

// display the date
const dateElement = document.getElementById("date");
let options = {
  weekday: "long",
  month: "short",
  day: "numeric"
};

let today = new Date();
dateElement.innerHTML = today.toLocaleDateString("en-US",options);

// create global variables
let LIST;
let id;

// constants of states
const CHECK = "fa-check-circle";
const UNCHECK = "fa-circle";
const LINE_THROUGH = "lineThrough";

/*--------------------- local storage -------------------*/

// get item from localstorage
let data = localStorage.getItem("TASK"); // restore array
if (data) {
  // if data exists (there r inputs) get array
  LIST = JSON.parse(data);
  id = LIST.length;
  //load list to the page
  loadTask(LIST);
}else {
  LIST = [];
  id = 0;
}

function loadTask(list){
  list.forEach(item=> {
    addTask(item.name,item.id,item.done,item.trash);
  });
}



/*---------------- add task to do ------------*/

function addTask(task,id,done,trash){
  // if trash is true skip the rest of the code and do nth else add task
  if (trash) {
    return;
  }
  // if task is done check else uncheck
  const DONE = done ? CHECK : UNCHECK;
  // if task is done draw line through task else draw nth
  const LINE = done ? LINE_THROUGH : "";
  const item = `<li class="item">
    <i class="fal complete ${DONE}" id="${id}" role="complete"></i>
    <p class="text ${LINE}">${task}</p>
    <i class="far fa-trash-alt  delete" role="delete" id="${id}"></i>
  </li> `;

  const position = "beforeend";
  list.insertAdjacentHTML(position,item);
}

document.addEventListener("keyup",function(event){
  // check if enter key is pressed
  if(event.keyCode == 13){
    // check if there is any input ready
    const toDo = input.value;
    if (toDo) {
      // if input value exists call addTask
      addTask(toDo,id,false,false);
      // add task to array
      LIST.push({
        name: toDo,
        id: id,
        done: false,
        trash: false
      });
    }
    // set item to localstorage whenever array is updated
    localStorage.setItem("TASK",JSON.stringify(LIST));
    id++;
    // else reset input value
    input.value = "";
  }
});


/*------------- when task is done ----------------*/
function completeTask(element){
  element.classList.toggle(CHECK);
  element.classList.toggle(UNCHECK);

  element.parentNode.querySelector(".text").classList.toggle(LINE_THROUGH);

  // update LIST
  LIST[element.id].done = LIST[element.id].done ? false : true;
}

/*-------------- remove task -----------------*/
function removeTask(element){
  element.parentNode.parentNode.removeChild(element.parentNode);
  LIST[element.id].trash = true;
}

list.addEventListener("click",function(event){
  // get element clicked in the list area
  const elt = event.target;
  let eltRole = null;
  if (elt.hasAttribute("role")) {
    eltRole = elt.attributes.role.value;
    if (eltRole == "complete") {
      completeTask(elt);
    }else if(eltRole == "delete"){
      removeTask(elt);
      }
    // set item to localstorage whenever array is updated
    localStorage.setItem("TASK",JSON.stringify(LIST));
  }
  });

  /*------------ clear localStorage ---------------------*/
  clear.addEventListener("click",function(){
    localStorage.clear();
    location.reload();
  });
